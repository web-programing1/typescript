//Members: Types
{
    class Person {
        name: string;
    }

    const person = new Person();
    person.name = "Jane";
}
//Members: Visibility - public, private, protected
{
    class Person {
        private name: string;

        public constructor(name: string) {
            this.name = name;
        }

        public getName(): string {
            return this.name;
        }
    }

    const person = new Person("Jane");
    console.log(person.getName()); // person.name isn't accessible from outside the class since it's private
}
//Parameter Properties
{
    class Person {
        // name is a private member variable
        public constructor(private name: string) { }

        public getName(): string {
            return this.name;
        }
    }

    const person = new Person("Jane");
    console.log(person.getName());
}
//Readonly
{
    class Person {
        private readonly name: string;

        public constructor(name: string) {
            // name cannot be changed after this initial definition, which has to be either at it's declaration or in the constructor.
            this.name = name;
        }

        public getName(): string {
            return this.name;
        }
    }

    const person = new Person("Jane");
    console.log(person.getName());

}
//Inheritance: Implements
{
    interface Shape {
        getArea: () => number;
    }

    class Rectangle implements Shape {
        public constructor(protected readonly width: number, protected readonly height: number) { }

        public getArea(): number {
            return this.width * this.height;
        }
    }
}
//Inheritance: Extends
{
    interface Shape {
        getArea: () => number;
    }

    class Rectangle implements Shape {
        public constructor(protected readonly width: number, protected readonly height: number) { }

        public getArea(): number {
            return this.width * this.height;
        }
    }

    class Square extends Rectangle {
        public constructor(width: number) {
            super(width, width);
        }

        // getArea gets inherited from Rectangle
    }
}
//Override
{
    interface Shape {
        getArea: () => number;
    }

    class Rectangle implements Shape {
        // using protected for these members allows access from classes that extend from this class, such as Square
        public constructor(protected readonly width: number, protected readonly height: number) { }

        public getArea(): number {
            return this.width * this.height;
        }

        public toString(): string {
            return `Rectangle[width=${this.width}, height=${this.height}]`;
        }
    }

    class Square extends Rectangle {
        public constructor(width: number) {
            super(width, width);
        }

        // this toString replaces the toString from Rectangle
        public override toString(): string {
            return `Square[width=${this.width}]`;
        }
    }
}
//Abstract
{
    abstract class Polygon {
        public abstract getArea(): number;

        public toString(): string {
            return `Polygon[area=${this.getArea()}]`;
        }
    }

    class Rectangle extends Polygon {
        public constructor(protected readonly width: number, protected readonly height: number) {
            super();
        }

        public getArea(): number {
            return this.width * this.height;
        }
    }
}
//Exercise
//Specify that Person.name can only be accessed within the class, but that the method Person.getName() can be accessed anywhere:
{
    class Person {

        private name: string;

        public constructor(name: string) {
            this.name = name;
        }

        public getName(): string {
            return this.name;
        }
    }
}