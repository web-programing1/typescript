//Union |
{
    function printStatusCode(code: string | number) { //parameter is a string or number
        console.log(`My status code is ${code}.`)
    }
    printStatusCode(404);
    printStatusCode('404');
}
//Union Type Errors
{
    function printStatusCode2(code: string | number) {
        console.log(`My status code is ${code.toUpperCase()}.`) // error: Property 'toUpperCase' does not exist ontype 'string | number'.
    }
}