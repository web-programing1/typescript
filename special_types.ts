let v: any = true;
v = "string"; // no error using 'any' keyword
Math.round(v); // no error using 'any' keyword

let w: unknown = 1;
w = "string"; // no error
w = {
  runANonExistentMethod: () => {
    console.log("I think therefore I am");
  }
} as { runANonExistentMethod: () => void}
if(typeof w === 'object' && w !== null) {
  (w as { runANonExistentMethod: Function }).runANonExistentMethod();
}

let x: never = true; // Error

let y: undefined = undefined; //javascript ndefined
let z: null = null; //javascript null

//Exercise
let myVar: any;