{
    enum CardinalDirections {
        North,
        East,
        South,
        West
    }
    let currentDirection = CardinalDirections.North;
    // logs 0
    console.log(currentDirection);
    // throws error as 'North' is not a valid enum
    currentDirection = 'North'; // Error: "North" is not assignable to type 'CardinalDirections'.
}
{
    enum CardinalDirections {
        North = 1,
        East,
        South,
        West
    }
    // logs 1
    console.log(CardinalDirections.North);
    // logs 4
    console.log(CardinalDirections.West);
}
{
    enum StatusCodes {
        NotFound = 404,
        Success = 200,
        Accepted = 202,
        BadRequest = 400
    }
    // logs 404
    console.log(StatusCodes.NotFound);
    // logs 200
    console.log(StatusCodes.Success);
}
{
    enum CardinalDirections {
        North = 'North',
        East = "East",
        South = "South",
        West = "West"
    };
    // logs "North"
    console.log(CardinalDirections.North);
    // logs "West"
    console.log(CardinalDirections.West);
}
{
    //Exercise
    //Create an enum called myEnum, with 2 constants (myFirstConst, mySecondConst) with default values:
    enum
        myEnum {

        myFirstConst
        ,

        mySecondConst

    };
}