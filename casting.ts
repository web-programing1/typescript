//Casting with as
{
    let x: unknown = 'hello';
    console.log((x as string).length);
}
//can not cast
{
    let x: unknown = 4;
    console.log((x as string).length); // prints undefined since numbers don't have a length
}
{
    console.log((4 as string).length); // Error: Conversion of type 'number' to type 'string'
}
//Casting with <>
{
    let x: unknown = 'hello';
    console.log((<string>x).length);
}
//Force casting
{
    let x = 'hello';
    console.log(((x as unknown) as number).length); // x is not actually a number so this will return undefined
}
//Excercise
//Cast the "unknown" variable myVar as a string, using the as keyword:
{
    let myVar: unknown = "Hello world!";
    console.log((myVar as string).length);
}